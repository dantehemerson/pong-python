import pygame
import sys
import os
import math
import random

class PongGame(object):
    WINDOW_WIDTH = 1000
    WINDOW_HEIGHT = 600
    FPS = 60 # Frames per Second
    game_over = False

    def __init__(self):
        self.pygame = pygame

    def init(self):
        self.pygame.init()
        self.windowSize = (self.WINDOW_WIDTH, self.WINDOW_HEIGHT)
        self.screen = pygame.display.set_mode(self.windowSize)
        self.clock = self.pygame.time.Clock()

    def run(self):
        while True:
            self.screen.fill((255, 255, 255))

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit(0)

            self.pygame.display.update()
            self.clock.tick(self.FPS)

if __name__ == '__main__':
    game = PongGame()
    game.init()
    game.run()
